package com.esprit.project;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ShopperRepository extends JpaRepository<Shopper , Integer> {
	@Query("select c from Shopper c where c.name like :name")
	public Page<Shopper> shopperByNom(@Param("name") String n, Pageable pageable);
}
