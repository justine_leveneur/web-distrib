package com.esprit.project;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@Entity
@EnableEurekaClient
public class Shopper implements Serializable {
    private static final long serialVersionUID = 6;
    public Shopper() {
		super();
	}
    public Shopper(String name, String firstname, String address, String email) {
        super();
        this.name = name;
        this.firstname = firstname;
        this.address = address;
        this.email = email;
    }

    public Shopper(String name) {
        super();
        this.name = name;
    }

    @Id 
    @GeneratedValue
    private int id;
    private String name, firstname, address, email;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
}
