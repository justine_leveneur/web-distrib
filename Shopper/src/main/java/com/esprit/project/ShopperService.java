package com.esprit.project;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopperService {

	@Autowired
	private ShopperRepository shopperRepository;

	public Shopper addShopper(Shopper shopper) {
		return shopperRepository.save(shopper);
	}

	public Shopper updateShopper(int id, Shopper newShopper) {
		if (shopperRepository.findById(id).isPresent()) {
			Shopper existingShopper = shopperRepository.findById(id).get();
			existingShopper.setName(newShopper.getName());
			existingShopper.setFirstname(newShopper.getFirstname());
			existingShopper.setEmail(newShopper.getEmail());

			return shopperRepository.save(existingShopper);
		} else
			return null;
	}

	public String deleteShopper(int id) {
		if (shopperRepository.findById(id).isPresent()) {
			shopperRepository.deleteById(id);
			return "shopper supprimé";
		} else
			return "shopper non supprimé";
	}
	
	public List<Shopper> findAll(){
		return shopperRepository.findAll();
	}

}