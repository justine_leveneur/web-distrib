package com.esprit.project;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController 
@RequestMapping(value = "/api/shopper")
public class ShopperRestAPI {
	private String title = "Bonjour, je suis un shopper Microservice"; 
	@Autowired
	private ShopperService shopperService;
	@RequestMapping("/hello")
	public String sayHello(){
		System.out.println(title);
		return title;
	}
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Shopper> createShopper(@RequestBody Shopper shopper) {
		return new ResponseEntity<>(shopperService.addShopper(shopper), HttpStatus.OK);
	}
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Shopper> updateShopper(@PathVariable(value = "id") int id,@RequestBody Shopper shopper) {
		return new ResponseEntity<>(shopperService.updateShopper(id, shopper), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> deleteShopper(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(shopperService.deleteShopper(id), HttpStatus.OK);
	}
	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Shopper>> getAll(){
		return new ResponseEntity<>(shopperService.findAll(), HttpStatus.OK);
	}
}


