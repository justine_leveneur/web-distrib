package com.esprit.project;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Snack implements Serializable  {
	private static final long serialVersionUID = 6;
    public Snack() {
		super();
	}

    public Snack(int quantity, String size, String snackName) {
		super();
		this.quantity = quantity;
		this.size = size;
		this.snackName = snackName;
	}

    public Snack(String snackName) {
        super();
        this.snackName = snackName;
    }
 
	@Id 
    @GeneratedValue
    private int id;
	private int quantity;
	private String snackName, size;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getSnackName() {
		return snackName;
	}
	public void setSnackName(String snackName) {
		this.snackName = snackName;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
}
