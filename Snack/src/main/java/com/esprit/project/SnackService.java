package com.esprit.project;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SnackService {

	@Autowired
	private SnackRepository snackRepository;
	
	public Snack addSnack(Snack snack) {
		return snackRepository.save(snack);
	}
	
	public Snack updateSnack(int id, Snack newSnack) {
		if (snackRepository.findById(id).isPresent()) {
			Snack existingSnack = snackRepository.findById(id).get();
			existingSnack.setSnackName(newSnack.getSnackName());
			existingSnack.setSize(newSnack.getSize());
			existingSnack.setQuantity(newSnack.getQuantity());

			return snackRepository.save(existingSnack);
		} else
			return null;
	}

	public String deleteSnack(int id) {
		if (snackRepository.findById(id).isPresent()) {
			snackRepository.deleteById(id);
			return "snack supprimé";
		} else
			return "snack non supprimé";
	}
	
	public List<Snack> findAll(){
		return snackRepository.findAll();
	}

}
