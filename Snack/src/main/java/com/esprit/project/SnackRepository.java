package com.esprit.project;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SnackRepository extends JpaRepository<Snack , Integer> {
	@Query("select c from Snack c where c.snackName like :snackName")
	public Page<Snack> shopperByNom(@Param("snackName") String n, Pageable pageable);
}
