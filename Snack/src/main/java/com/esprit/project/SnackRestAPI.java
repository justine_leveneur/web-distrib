package com.esprit.project;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/snack")
public class SnackRestAPI {

	private String title = "Bonjour, je suis un Microservice de snack"; 
	@Autowired
	private SnackService snackService;
	@RequestMapping("/hello")
	public String sayHello(){
		System.out.println(title);
		return title;
	}
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Snack> createSnack(@RequestBody Snack snack) {
		return new ResponseEntity<>(snackService.addSnack(snack), HttpStatus.OK);
	}
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Snack> updateSnack(@PathVariable(value = "id") int id,@RequestBody Snack snack) {
		return new ResponseEntity<>(snackService.updateSnack(id, snack), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> deleteSnack(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(snackService.deleteSnack(id), HttpStatus.OK);
	}
	@GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Snack>> getAll(){
		return new ResponseEntity<>(snackService.findAll(), HttpStatus.OK);
	}
}
