package com.esprit.projet;

import org.springframework.beans.factory.annotation.Autowired;

public class PurchaseService {

	@Autowired
	private PurchaseRepository purchaseRepository;

	public Purchase addPurchase(Purchase purchase) {
		return purchaseRepository.save(purchase);
	}
	
	public String deletePurchase(int id) {
		if (purchaseRepository.findById(id).isPresent()) {
			purchaseRepository.deleteById(id);
			return "purchase supprimé";
	}else 
		return "purchase non supprimé";
	}
}
