package com.esprit.projet;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@Entity
@EnableEurekaClient
public class Purchase implements Serializable{
	private static final long serialVersionUID = 6;
	public Purchase() {
		super();
	}
	public Purchase(String name, Date date) {
        super();
        this.name = name;
        this.date = date;
    }

	public Purchase(String name) {
        super();
        this.name = name;
    }
	
    @Id 
    @GeneratedValue
    private int id;
    private String name;
    private Date date;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
}
