package com.esprit.projet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

public class PurchaseRestAPI {

	private String title = "Bonjour, je suis un purchase Microservice"; 
	@Autowired
	private PurchaseService purchaseService;
	@RequestMapping("/hello")
	public String sayHello(){
		System.out.println(title);
		return title;
	}
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Purchase> createPurchase(@RequestBody Purchase purchase) {
		return new ResponseEntity<>(purchaseService.addPurchase(purchase), HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> deletePurchase(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(purchaseService.deletePurchase(id), HttpStatus.OK);
	}
}
