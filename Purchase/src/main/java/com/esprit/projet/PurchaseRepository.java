package com.esprit.projet;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer>{
	@Query("select c from Purchase c where c.name like :name")
	public Page<Purchase> purchaseByNom(@Param("name") String n, Pageable pageable);
}
