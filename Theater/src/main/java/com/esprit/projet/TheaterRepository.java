package com.esprit.projet;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TheaterRepository extends JpaRepository<Theater, Integer>{
	@Query("select c from Theater c where c.name like :name")
	public Page<Theater> shopperByNom(@Param("name") String n, Pageable pageable);
}
