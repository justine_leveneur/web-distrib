package com.esprit.projet;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@Entity
public class Theater implements Serializable {

	private static final long serialVersionUID = 6;
    public Theater() {
		super();
	}
    public Theater(String name, String ville) {
        super();
        this.name = name;
        this.ville = ville;
    }

    public Theater(String name) {
        super();
        this.name = name;
    }

    @Id 
    @GeneratedValue
    private int id;
    private String name, ville;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getVille() {
        return ville;
    }
    public void setVille(String ville) {
        this.ville = ville;
    }
}
