package com.esprit.projet;

import java.util.stream.Stream;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
public class TheaterApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheaterApplication.class, args);
	}
	
	@Bean
    ApplicationRunner init (TheaterRepository repository) {
        return args -> {
            Stream.of("name", "ville").forEach(name -> {
            	repository.save(new Theater(name));
            });
            repository.findAll().forEach(System.out::println);
        };
	}
}
