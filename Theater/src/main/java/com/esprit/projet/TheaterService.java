package com.esprit.projet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TheaterService {

	@Autowired
	private TheaterRepository theaterRepository;

	public Theater addTheater(Theater theater) {
		return theaterRepository.save(theater);
	}
	
	public Theater updateTheater(int id, Theater newTheater) {
		if (theaterRepository.findById(id).isPresent()) {
			Theater existingTheater = theaterRepository.findById(id).get();
			existingTheater.setName(newTheater.getName());
			existingTheater.setVille(newTheater.getVille());
				
			return theaterRepository.save(existingTheater);
		} else {
			return null;
		}
	}
	
	public String deleteTheater(int id) {
		if (theaterRepository.findById(id).isPresent()) {
			theaterRepository.deleteById(id);
			return "theater supprimé";
		}else {
			return "theater non supprimé";
		}
	}
}
