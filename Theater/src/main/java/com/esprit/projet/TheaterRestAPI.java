package com.esprit.projet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController 
@RequestMapping(value = "/api/theater")
public class TheaterRestAPI {
	
	private String title = "Bonjour, je suis un theater Microservice"; 
	@Autowired
	private TheaterService theaterService;
	@RequestMapping("/hello")
	public String sayHello(){
		System.out.println(title);
		return title;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Theater> createTheater(@RequestBody Theater theater) {
		return new ResponseEntity<>(theaterService.addTheater(theater), HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Theater> updateTheater(@PathVariable(value = "id") int id,@RequestBody Theater theater) {
		return new ResponseEntity<>(theaterService.updateTheater(id, theater), HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> deleteTheater(@PathVariable(value = "id") int id){
		return new ResponseEntity<>(theaterService.deleteTheater(id), HttpStatus.OK);
	}
}
