var express = require("express");

var app = express();

var bodyParser = require('body-parser')
 
var jsonParser = bodyParser.json()

const movies = require('./movies.json')

// Or, if you're not using a transpiler:
const Eureka = require('eureka-js-client').Eureka;


app.listen(8791);

app.get('/api/movies', (req,res) => {
    res.status(200).json(movies)
})

app.get('/api/movies/:id', (req,res) => {
    const id = parseInt(req.params.id)
    const movie = movies.find(movie => movie.id === id)
    res.status(200).json(movie)
})

app.post('/api/movies',jsonParser, (req,res) => {
    console.log(req.body);
    if(movies.index === undefined){
        movies.index = 0;
    }
    let movie = req.body;
    movie.id=movies.index++;
    movies.push(req.body)
    res.status(200).json(movies)
})

app.put('/api/movies/:id', (req,res) => {
    const id = parseInt(req.params.id)
    let movie = movies.find(movie => movie.id === id)
    movie.dateSortie =req.body.dateSortie,
    movie.dateFin =req.body.dateFin,
    movie.name =req.body.name,
    res.status(200).json(movie)
})

app.delete('/api/movies/:id', (req,res) => {
    const id = parseInt(req.params.id)
    let movie = movies.find(movie => movie.id === id)
    movies.splice(movies.indexOf(movie),1)
    res.status(200).json(movies)
})
